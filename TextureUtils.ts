export class TextureData {
  size;
  heigth;
  width;
  data: Uint8Array;

  constructor(textureSize) {
    this.size = textureSize;
    this.heigth = this.size;
    this.width = this.size;
    this.data = new Uint8Array(this.heigth * this.width * 3);
  }

  clear() {
    this.data.forEach((v, i) => (this.data[i] = 0));
  }

  drawPointAt(row, col, val) {
    let index = row * this.width + col;

    this.data[index] = val;
  }

  fillTexture(rawData) {
    rawData.forEach((val, i) => {
      // val = 0.2;
      this.data[i * 3] = val;
      this.data[i * 3 + 1] = val;
      this.data[i * 3 + 2] = val;
    });
  }
}
